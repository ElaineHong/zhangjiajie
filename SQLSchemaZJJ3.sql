CREATE TABLE `package_types` (
  `id` int(11) NOT NULL,
  `package_id` int(11) DEFAULT NULL,
  `days_cnt` int(11) DEFAULT NULL,
  `age_group` enum('A','C','S') DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PKG_ID2_idx` (`package_id`),
  CONSTRAINT `FK_PKG_ID2` FOREIGN KEY (`package_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `coverpic` varchar(250) DEFAULT NULL,
  `about` varchar(200) DEFAULT NULL,
  `details` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

CREATE TABLE `packages_gallery` (
  `id` int(11) NOT NULL,
  `packages_id` int(11) NOT NULL,
  `photo` blob NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PCKG_PHOTO_idx` (`packages_id`),
  CONSTRAINT `FK_PCKG_PHOTO` FOREIGN KEY (`packages_id`) REFERENCES `packages` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `prices` (
  `id` int(11) NOT NULL,
  `packages_type_id` int(11) DEFAULT NULL,
  `class_type` enum('1','2','3','4') DEFAULT NULL,
  `priceperpax` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_PCG_TYP_ID_idx` (`packages_type_id`),
  CONSTRAINT `FK_PCG_TYP_ID` FOREIGN KEY (`packages_type_id`) REFERENCES `package_types` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



