var path = require("path");
var express = require("express");
var mysql = require("mysql");
var q = require("q");

var app = express();

var passport = require("passport");
var bodyParser = require("body-parser");
var session = require("express-session");
var flash = require('connect-flash');
var cookieParser = require('cookie-parser');

const PORT = "port";

app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

var pool = mysql.createPool({
    host: "localhost",
    port: 3306,
    user: "elainehong",
    password: "appdeveloper88",
    database: "zjj3",
    connectionLimit: 4
});

const findOnePackage = "SELECT packages.name, packages.day1 ,packages.day2, packages.day3,packages.day4,packages.photo1,packages.photo2" +
    " FROM packages" +
    " WHERE id = ?"

const findPackageNameAgePrice = "SELECT packages.id, packages.name," +
    " MAX(IF(package_types.age_group = 'A', prices.priceperpax, NULL)) adultprice," +
    " MAX(IF(package_types.age_group = 'C', prices.priceperpax, NULL)) childprice," +
    " MAX(IF(package_types.age_group = 'S', prices.priceperpax, NULL)) seniorprice" +
    " FROM packages" +
    " JOIN package_types" +
    " ON packages.id = package_types.package_id" +
    " JOIN prices" +
    " ON package_types.id = prices.packages_type_id" +
    " WHERE class_type = '1'" +
    " GROUP BY packages.id"

const saveOneBooking = "INSERT INTO bookings (packages_id, specialRequest, arrivalTransport, arrivalDate, arrivalTime," +
    " flightTrainNo, travellerTitle, travellerFirstName, travellerFamilyName, travellerNationality, travellerPhoneNumber1," +
    " travellerPhoneNumber2, travellerEmail,paymentType, noOfSenior, noOfAdult, noOfChild, paymentAmt)" +
    " VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"


var makeQuery = function (sql, pool) {
    return function (args) {
        var defer = q.defer();

        console.log("=== in makeQuery === ");
        console.log("=== sql === " + sql);
        pool.getConnection(function (err, conn) {
            if (err) {
                defer.reject(err);
                console.log("error from conn" + err);
                return;
            }
            console.log("arguments: " + args);
            conn.query(sql, args || [], function (err, results) {
                conn.release();
                if (err) {
                    console.log("error from query" + err);
                    defer.reject(err);
                    return;
                }
                console.log("results: " + results);
                defer.resolve(results);
            });
        });
        return defer.promise;
    }
};

var findOne = makeQuery(findOnePackage, pool);
var findPNAP = makeQuery(findPackageNameAgePrice, pool);
var saveOne = makeQuery(saveOneBooking, pool);


// Start of the route to details Page
app.get("/api/package/:packageId", function (req, res) {
    console.log("in /api/package/:packageId ... packageID " + req.params.packageId);
    findOne([req.params.packageId])
        .then(function (results) {
            console.log("results from DB: " + JSON.stringify(results));
            res.status(200).json(results[0]);
        })
        .catch(function (err) {
            console.log("error returned: " + err);
            res.status(500).end();
        });
});

// Start of the route to getPackages 

app.get("/api/packages", function (req, res) {
    findPNAP()
        .then(function (results) {
            console.log("results from DB: " + JSON.stringify(results));
            res.status(200).json(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });

});

// Start of the route to save one booking to Database
app.post("/api/booking/save", function (req, res) {
    console.info(req.body.params);
    saveOne([req.body.params.package_id, req.body.params.specialRequest, req.body.params.arrivalTime, req.body.params.arrivalDate, req.body.params.arrivalTime, req.body.params.flightTrainNo, req.body.params.travellerTitle, req.body.params.travellerFirstName, req.body.params.travellerFamilyName, req.body.params.travellerNationality, req.body.params.travellerPhoneNumber1, req.body.params.travellerPhoneNumber2, req.body.params.travellerEmail, req.body.params.paymentType, req.body.params.noOfSenior, req.body.params.noOfAdult, req.body.params.noOfChild, req.body.params.paymentAmt])
        .then(function (results) {
            res.status(200).json(results);
            console.log(results);
        })
        .catch(function (err) {
            res.status(500).end();
        });
});


// Initialize session
app.use(session({
    secret: "nus-iss-stackup",
    resave: false,
    saveUninitialized: true
}));

//Initialize passport
app.use(passport.initialize());
app.use(passport.session());

// connect flash
app.use(flash());

require('./auth.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./routes.js')(app); // load our routes and pass in our app and fully configured passport

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3000);

app.listen(app.get(PORT), function () {
    console.info("App Server started on " + app.get(PORT));
});
