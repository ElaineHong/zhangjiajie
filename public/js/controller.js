(function () {
    angular
        .module("ZJJApp")
        .controller("HomeCtrl", HomeCtrl)
        .controller("PackageCtrl", PackageCtrl)
        .controller("BookingCtrl", BookingCtrl)
        .controller("RegisterCtrl", RegisterCtrl)
        .controller("LoginCtrl", LoginCtrl)
        .controller("LogoutCtrl", LogoutCtrl);

    HomeCtrl.$inject = ["dbService", "psService", "$state"];
    function HomeCtrl(dbService, psService, $state) {
        var vm = this; // create "vm" object and assign "this" object to it

        vm.vacations = {}; // attach "vacations" string to vm object

        
        initHome(); // Preload home page with info


        // Initialize Home page
        function initHome() {
            // call service that would read packages and prices

            dbService.getVacation()
                .then(function (results) {
                    console.log("result from DB " + JSON.stringify(results));
                    vm.vacations = results;
                    console.log("vm.vacations " + JSON.stringify(vm.vacations));
                })
                .catch(function (err) {
                    console.info("Some Error Occurred", err)
                });
        }


        // attach "getPackage" function to vm object
        vm.getPackage = function (id) {
            console.log("id sent by home.html " + id);
            $state.go("itinerary", {"packageId": id});
        };

        vm.booking = function (package) {
            console.log("selected package: " + package);
            psService.setSelectedPackage(package);
            // $state.go("booking",{"packageId":package.id});
            $state.go("booking");
        };
        
        //Reprice functions

        vm.seniorQty = 1;
        vm.adultQty = 1;
        vm.childQty = 1;

        vm.reprice= function(seniorPrice,adultPrice, childPrice, type){
            vm.totalSeniorPrice = 0;
            vm.totalAdultPrice = 0;
            vm.totalChildPrice = 0;
            vm.totalSeniorPrice = seniorPrice * vm.seniorQty;
            vm.totalAdultPrice = adultPrice * vm.adultQty;
            vm.totalChildPrice= childPrice*vm.childQty;
            vm.totalPrice=vm.totalSeniorPrice +vm.totalAdultPrice + vm.totalChildPrice;
        };
        
    }

    PackageCtrl.$inject = ["dbService", "psService", "$stateParams", "$state"];
    function PackageCtrl(dbService, psService, $stateParams, $state) {
        var vm = this;

        vm.package = {};
        console.log("packageID : " + JSON.stringify($stateParams));

        dbService.display($stateParams.packageId)
            .then(function (package) {
                console.log("package from DB " + package);
                vm.package = package;
                console.log("vm.package " + vm.package);
            })
            .catch(function (err) {
                console.info("Some Error Occurred", err)
            });

        vm.booking = function (package) {
            console.log("selected package: " + package);
            psService.setSelectedPackage(package);
            // $state.go("booking",{"packageId":package.id});
            $state.go("booking");
        };

    }


    // var ctrl = this;
    // ctrl.booking = {};
    // ctrl.booking.packages_id = "";
    // ctrl.booking.specialRequest = "";
    // ctrl.booking.arrivalTransport = "";
    // ctrl.booking.arrivalDate = "";
    // ctrl.booking.arrivalTime = "";
    // ctrl.booking.flightTrainNo = "";
    // ctrl.booking.travellerTitle = "";
    // ctrl.booking.travellerFirstName = "";
    // ctrl.booking.travellerFamilyName = "";
    // ctrl.booking.travellerNationality = "";
    // ctrl.booking.travellerPhoneNumber1 = "";
    // ctrl.booking.travellerPhoneNumber2 = "";
    // ctrl.booking.travellerEmail = "";
    // ctrl.booking.paymentType = "";
    // ctrl.booking.noOfSenior = "";
    // ctrl.booking.noOfAdult = "";
    // ctrl.booking.noOfChild = "";
    // ctrl.booking.paymentAmt = "";
    //

    BookingCtrl.$inject = ["dbService", "psService", "$stateParams", "$state"];
    function BookingCtrl(dbService, psService, $stateParams, $state) {
        var vm = this;
        vm.selectedPackage = {};


        console.log("-- in BookingCtrl -- ");
        vm.selectedPackage = psService.getSelectedPackage();
        console.log("get selected package using psService" +
            JSON.stringify(psService.getSelectedPackage()));


        vm.buy = function () {
            console.log("-- BookingCtrl.buy() -- ");
            $state.go("comfirmationPage")
            dbService.saveBooking()
                .then(function () {
                    $state.go("comfirmationPage");
                    console.info("Success");
                    ctrl.status.message = "The booking is added to the database";
                    ctrl.status.code = 202;
                })
                .catch(function () {
                    console.info("Error");
                    ctrl.status.message = "Failed to add the booking to the database";
                    ctrl.status.code = 400;
                });
        };
    };


    RegisterCtrl.$inject = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService", "Flash"];
    function RegisterCtrl($http, $q, $sanitize, $state, dbService, AuthService, Flash) {
        var vm = this;
        vm.emailAddress = "";
        vm.password = "";
        vm.confirmpassword = "";

        vm.register = function () {
            AuthService.register($sanitize(vm.emailAddress), $sanitize(vm.password))
                .then(function () {
                    vm.disabled = false;

                    vm.emailAddress = "";
                    vm.password = "";
                    vm.confirmpassword = "";
                    Flash.clear();
                    Flash.create('success', "Successfully sign up with us, Please proceed to login", 0, {
                        class: 'custom-class',
                        id: 'custom-id'
                    }, true);
                    $state.go("SignIn");
                }).catch(function () {
                console.error("registration having issues");
            });
        };

    } // end  RegisterCtrl

    function LoginCtrl($http, $q, $sanitize, $state, dbService, AuthService, Flash) {
        var vm = this;

        vm.login = function () {
            AuthService.login(vm.user)
                .then(function () {
                    if (AuthService.isLoggedIn()) {
                        vm.emailAddress = "";
                        vm.password = "";
                        $state.go("ZJJApp");
                    } else {
                        Flash.create('danger', "Ooops having issue logging in!", 0, {
                            class: 'custom-class',
                            id: 'custom-id'
                        }, true);
                        $state.go("SignIn");
                    }
                }).catch(function () {
                console.error("Error logging on !");
            });
        };
    }

    LoginCtrl.$inject = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService", "Flash"];

    function LogoutCtrl($http, $q, $sanitize, $state, dbService, AuthService, Flash) {
        var vm = this;

        vm.logout = function () {
            AuthService.logout()
                .then(function () {
                    $state.go("SignIn");
                }).catch(function () {
                console.error("Error logging on !");
            });
        };
    }

    LogoutCtrl.$inject = ["$http", "$q", "$sanitize", "$state", "dbService", "AuthService", "Flash"];


})();