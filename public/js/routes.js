(function () {

    angular
        .module("ZJJApp")
        .config(ZJJConfig);

    ZJJConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    function ZJJConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state("home", {
                url: "/home",
                templateUrl: "/views/home.html",
                controller: "HomeCtrl as ctrl"

            })


            .state("SignIn", {
                url: "/signIn",
                templateUrl: "../views/login.html",
                controller: "LoginCtrl as ctrl"
            })
            .state("SignUp", {
                url: "/signUp",
                templateUrl: "../views/register.html",
                controller: "RegisterCtrl as ctrl"
            })
            .state("MyAccount", {
                url: "/MyAccount",
                templateUrl: "views/profile.html", 
                controller: "MyAccountCtrl as ctrl"
            })

            .state("booking", {
                // url: "/booking/:packageId",
                url: "/booking",
                templateUrl: "/views/booking.html",
                controller:"BookingCtrl as ctrl"
            })

            .state("itinerary", {
                url:"/itinerary/:packageId",
                templateUrl: "/views/itinerary.html",
                controller:"PackageCtrl as ctrl"
            })


            .state("budget_summary", {
                url:"/budget_summary",
                templateUrl: "/views/budget_summary.html"
                
            })
            
            .state("comfirmationPage", {
                url: "/comfirmationPage",
                templateUrl: "/views/comfirmationPage.html"
                
            });
        $urlRouterProvider.otherwise("/home");
    }
})();


