(function () {
    angular.module("ZJJApp")
        .service("dbService", dbService)
        .service("psService", psService);

    dbService.$inject = ["$http", "$q"];
    function dbService($http, $q) {
        var service = this;

        service.getLocalProfile = function (callback) {
            var defer = $q.defer();
            $http.get("/api/user/profile/")
                .then(function (result) {
                    defer.resolve(result);
                }).catch(function (error) {
                defer.reject(error);
            });
            return defer.promise;
        };
        
        service.display = function(packageId){
            var defer =$q.defer();
            $http.get("/api/package/"+packageId)
                .then(function(result){
                    defer.resolve(result.data);
                })
                .catch(function(err){
                    defer.reject(err);
                });
            return defer.promise;
            
        };

        service.getVacation = function(){
            var defer =$q.defer();
            console.log("--- in service.getVacation ---");
            
            $http.get("/api/packages")
                .then(function(result){
                    defer.resolve(result.data);
                })
                .catch(function(err){
                    defer.reject(err);
                });
            return defer.promise;

        };
        
        service.saveBooking= function(data){
            var defer= $q.defer();
            $http.post("/api/booking/save", data)
                .then(function(result){
                    defer.resolve(result.data);
                })
                .catch(function(err){
                    defer.reject(error.status);
                });
            return defer.promise;
        };
               
    } // END dbService


    /* psService service is used to pass variables to different states */
    psService.$inject = [];
    function psService() {
        var service = this;
        // service.selectedPackage = {};  // variable for selected package
        var selectedPackage = {};

        
        service.setSelectedPackage = function (package) {
            selectedPackage  = package;
            console.log("--setting this package: " + JSON.stringify(selectedPackage));
        }; // END service.selectedPackage

        service.getSelectedPackage = function () {
            return(selectedPackage);
        } // END service.getSelectedPackage
    } // end passingService()
    })();