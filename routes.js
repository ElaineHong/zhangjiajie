
var models = require('./models/index');
var express = require("express");
var bcrypt   = require('bcrypt-nodejs');
var passport = require('passport');
var q = require("q");
var _ = require("underscore");
//
module.exports = function(app) {
//
//     const MAX_PAGE_PER_RECORD = 50;

    // app.get("/api/defaultapp/totalcnt/", isAuthenticated, function (req, res) {
    //     models.default_app.findAndCountAll({})
    //         .then(function(result) {
    //             res.status(200).json(result.count);
    //         }).catch(function (err) {
    //         console.error(err);
    //         res.status(500).end();
    //     });
    //
    // });

    app.get("/api/user/profile/", isAuthenticated, function (req, res) {
        models.users.findOne({where: {email: req.user.email}})
            .then(function(result) {
                res.status(200).json(result);
            }).catch(function (err) {
            console.error(err);
            res.status(500).end();
        });

    });

    // app.get("/api/user/social/profiles", isAuthenticated, function (req, res) {
    //     models.authentication_provider.findAll({where: {userId: req.user.id}})
    //         .then(function(result) {
    //             res.status(200).json(result);
    //         }).catch(function (err) {
    //         console.error(err);
    //         res.status(500).end();
    //     });
    // });

//     app.get("/api/defaultapp/pagination/:offset/:pgrecord", isAuthenticated,function (req, res) {
//         var offset = req.params.offset;
//         var noOfRecPerPage = req.params.pgrecord;
//
//         if (noOfRecPerPage >= 50) {
//             noOfRecPerPage = MAX_PAGE_PER_RECORD
//         }
//
//         models.default_app.findAll({ offset: parseInt(offset), limit: parseInt(noOfRecPerPage) })
//             .then(function(apps) {
//                 res.status(200).json(apps);
//             }).catch(function (err) {
//     console.error(err);
//     res.status(500).end();
// });
// });


    // app.get("/api/customapp/totalcnt/", isAuthenticated, function (req, res) {
    //     models.custom_app.findAndCountAll({})
    //         .then(function(result) {
    //             res.status(200).json(result.count);
    //         }).catch(function (err) {
    //         console.error(err);
    //         res.status(500).end();
    //     });
    // });

    // app.get("/api/customapp/pagination/:offset/:pgrecord", isAuthenticated, function (req, res) {
    //     var offset = req.params.offset;
    //     var noOfRecPerPage = req.params.pgrecord;
    //
    //     if (noOfRecPerPage >= 50) {
    //         noOfRecPerPage = MAX_PAGE_PER_RECORD
    //     }
    //
    //     models.custom_app.findAll({ offset: parseInt(offset), limit: parseInt(noOfRecPerPage) })
    //         .then(function(apps) {
    //             res.status(200).json(apps);
    //         }).catch(function (err) {
    //         console.error(err);
    //         res.status(500).end();
    //     });
    // });

    app.post('/register', function(req, res) {
        if(!req.body.password === req.body.confirmpassword) {
            return res.status(500).json({
                err: err
            });
        }

        var hashpassword = bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(8), null);
        models.users.findOrCreate({where: {email: req.body.username,},defaults: { email: req.body.username,  encrypted_password: hashpassword}})
            .spread(function(user, created) {
                if(created){
                    user.encrypted_password = "";
                    res.status(200);
                    returnResults(user,res);
                }else{
                    user.encrypted_password = "";
                    res.status(500);
                    returnResults(user,res);
                }
            }).error(function(error){
            res.status(500);
            returnResults(error,res);
        });
    });


    app.get('/home', isAuthenticated, function(req, res) {
        res.redirect('../');
    });

    app.post("/login", passport.authenticate("local", {
        successRedirect: "views/booking",
        failureRedirect: "/",
        failureFlash : true
    }));

    // app.get("/oauth/google", passport.authenticate("google", {
    //     scope: ["email", "profile"]
    // }));

    // app.get("/oauth/facebook", passport.authenticate("facebook", {
    //     scope: ["email", "public_profile"]
    // }));

    // app.get("/oauth/google/callback", passport.authenticate("google", {
    //     successRedirect: "/home",
    //     failureRedirect: "/signUp"
    // }));

    // app.get("/oauth/facebook/callback", passport.authenticate("facebook", {
    //     successRedirect: "/home",
    //     failureRedirect: "/signUp",
    //     failureFlash : true
    // }));

    // app.get('/oauth/linkedin',
    //     passport.authenticate('linkedin', { state: 'SOME STATE'  }),
    //     function(req, res){
    //         // The request will be redirected to LinkedIn for authentication, so this
    //         // function will not be called.
    //     });

    // app.get('/oauth/linkedin/callback', passport.authenticate('linkedin', {
    //     successRedirect: '/home',
    //     failureRedirect: '/signUp',
    //     failureFlash : true
    // }));
    //
    // app.get('/oauth/wechat', passport.authenticate('wechat'));
    //
    // app.get('/oauth/wechat/callback', passport.authenticate('wechat', {
    //     failureRedirect: '/signUp',
    //     successReturnToOrRedirect: '/',
    //     failureFlash : true
    // }));
    //
    // app.get('/oauth/twitter', passport.authenticate('twitter'));
    //
    // // handle the callback after twitter has authenticated the user
    // app.get('/oauth/twitter/callback',
    //     passport.authenticate('twitter', {
    //         successRedirect : '/home',
    //         failureRedirect : '/signUp'
    //     }));

    app.get("/status/user", function (req, res) {
        var status = "";
        if(req.user) {
            status = req.user.email;
        }
        console.info("status of the user ??? " + status);
        res.send(status).end();
    });

    app.get("/logout", function(req, res) {
        req.logout();             // clears the passport session
        req.session.destroy();    // destroys all session related data
        res.send(req.user).end();
    });

    function isAuthenticated(req, res, next) {
        if (req.isAuthenticated())
            return next();
        res.redirect('/');
    }

    function returnResults(results, res) {
        res.send(results);
    }

};

